// Scss entrypoint
import "../Scss/app.scss";

// Include sprite images
import "../Image/Icon/typo3.svg";

// Javascript modules
import { Sprite } from "./sprite";

console.log("Welcome to TYPO3");

new Sprite();
