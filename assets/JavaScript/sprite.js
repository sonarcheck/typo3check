export class Sprite {
  constructor(selector = '#image-sprite') {
    const iconSpriteUrl = document.querySelector(selector).dataset.url

    fetch(iconSpriteUrl)
      .then((res) => {
        return res.text();
      })
      .then((data) => {
        let icons = document.createElement("div");
        icons.id = "svg-icons";
        icons.style.display = "none";
        icons.innerHTML = data;
        document.body.appendChild(icons);
      }).catch((e) => {
      console.log(e.message)
    });
  }
}
